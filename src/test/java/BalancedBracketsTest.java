import org.junit.Test;
import static org.junit.Assert.*;

public class BalancedBracketsTest {

    @Test
    public void testBalancedBrackets1() {
        BalancedBrackets balancedBrackets = new BalancedBrackets();
        assertEquals(true, balancedBrackets.hasBalancedBrackets("[LaunchCode]"));
    }

    @Test
    public void testBalancedBrackets2() {
        BalancedBrackets balancedBrackets = new BalancedBrackets();
        assertEquals(true, balancedBrackets.hasBalancedBrackets("Launch[Code]"));
    }

    @Test
    public void testBalancedBrackets3() {
        BalancedBrackets balancedBrackets = new BalancedBrackets();
        assertEquals(true, balancedBrackets.hasBalancedBrackets("[]LaunchCode"));
    }

    @Test
    public void testBalancedBrackets4() {
        BalancedBrackets balancedBrackets = new BalancedBrackets();
        assertEquals(true, balancedBrackets.hasBalancedBrackets(""));
    }

    @Test
    public void testBalancedBrackets5() {
        BalancedBrackets balancedBrackets = new BalancedBrackets();
        assertEquals(true, balancedBrackets.hasBalancedBrackets("[]"));
    }

    @Test
    public void testBalancedBrackets11() {
        BalancedBrackets balancedBrackets = new BalancedBrackets();
        assertNotEquals(true, balancedBrackets.hasBalancedBrackets("[LaunchCode"));
    }

    @Test
    public void testBalancedBrackets12() {
        BalancedBrackets balancedBrackets = new BalancedBrackets();
        assertNotEquals(true, balancedBrackets.hasBalancedBrackets("Launch]Code["));
    }

    @Test
    public void testBalancedBrackets13() {
        BalancedBrackets balancedBrackets = new BalancedBrackets();
        assertNotEquals(true, balancedBrackets.hasBalancedBrackets("["));
    }

    @Test
    public void testBalancedBrackets14() {
        BalancedBrackets balancedBrackets = new BalancedBrackets();
        assertNotEquals(true, balancedBrackets.hasBalancedBrackets("]["));
    }

    @Test
    public void testBinarySearch1() {
        int[] testArray = {1,2,3,4,5};
        BinarySearch binarySearch = new BinarySearch();
        assertEquals(4, binarySearch.binarySearch(testArray,5),.001);
    }

    @Test
    public void testBinarySearch2() {
        int[] testArray = {1,2,3,4,5};
        BinarySearch binarySearch = new BinarySearch();
        assertNotEquals(1, binarySearch.binarySearch(testArray,3),.001);
    }

    @Test
    public void testFractionGetNumerator1 () {
        Fraction fraction = new Fraction(2,3);
        assertEquals(2, fraction.getNumerator(),.001);
    }

    @Test
    public void testFractionGetNumerator2 () {
        Fraction fraction = new Fraction(4);
        assertEquals(4, fraction.getNumerator(),.001);
    }

    @Test
    public void testFractionGetDenominator1 () {
        Fraction fraction = new Fraction(2,3);
        assertEquals(3, fraction.getDenominator(),001);
    }

    @Test
    public void testFractionGetDenominator2 () {
        Fraction fraction = new Fraction(4);
        assertEquals(1, fraction.getDenominator(),001);
    }

    @Test
    public void testFractionAdd1() {
        Fraction fraction = new Fraction(2, 3);
        Fraction newFraction = new Fraction(4, 5);
        Fraction sumFraction = fraction.add(newFraction);
        assertEquals(22, sumFraction.getNumerator(), .001);
    }

    @Test
    public void testFractionAdd2() {
        Fraction fraction = new Fraction(2, 3);
        Fraction newFraction = new Fraction(4, 5);
        Fraction sumFraction = fraction.add(newFraction);
        assertEquals(15, sumFraction.getDenominator(), .001);
    }

    @Test
    public void testFractionAdd3() {
        Fraction fraction = new Fraction(2, 3);
        Integer integer = new Integer(1);
        Fraction sumFraction = fraction.add(integer);
        assertEquals(5, sumFraction.getNumerator(), .001);
    }

    @Test
    public void testFractionAdd4() {
        Fraction fraction = new Fraction(2, 3);
        Integer integer = new Integer(1);
        Fraction sumFraction = fraction.add(integer);
        assertEquals(3, sumFraction.getDenominator(), .001);
    }

    @Test
    public void testFractionToString() {
        Fraction fraction = new Fraction(2, 3);
        assertEquals("2/3", fraction.toString());
    }

    @Test
    public void testFractionCompare() {
        Fraction fraction = new Fraction(2, 3);
        Fraction other = new Fraction(3,4);
        assertEquals(-1, fraction.compareTo(other));
    }

}
